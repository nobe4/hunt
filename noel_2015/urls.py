from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^login/$',
        auth_views.login,
        {'template_name': 'login.html'}, name='login' ),
    url(r'^', include('hunt.urls')),
    url(r'^admin/', admin.site.urls),
]
