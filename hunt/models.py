from __future__ import unicode_literals
from django.db import models
from django.contrib.auth import models as authModels
from django.conf import settings

class Test(models.Model):
    title = models.CharField(max_length=200, default="")
    text = models.TextField()
    order = models.IntegerField(unique=True, default=-1)
    question = models.CharField(max_length=200, default="")
    answer = models.CharField(max_length=20, default="")
    def __str__(self):
        return self.title

class TestPart(models.Model):
    test = models.ForeignKey(Test)
    text = models.TextField()
    order = models.IntegerField(default=-1)
    question = models.CharField(max_length=200, default="")
    answer = models.CharField(max_length=20, default="")

    def __str__(self):
        return "[%s %s] %s (%s : %s)" % \
                (self.test, \
                 self.order, \
                 self.text[:75] + '..', \
                 self.question, \
                 self.answer)

class TestAnswer(models.Model):
    test = models.ForeignKey(Test)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    time = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "%s %s (%s)" % \
                (self.test, \
                 self.user, \
                 self.time)

class TestPartAnswer(models.Model):
    testPart = models.ForeignKey(TestPart)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    time = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "%s %s (%s)" % \
                (self.testPart, \
                 self.user, \
                 self.time)

