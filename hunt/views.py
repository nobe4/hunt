from django.shortcuts import render, redirect
import datetime
from django.contrib.auth.decorators import login_required
from .models import Test, TestPart, TestAnswer, TestPartAnswer
from django.contrib.auth.models import User

def login(request):
    return render(request, "login.html")

def validateField(userId, fieldName, value):
    fieldNameParts = fieldName.split('-')
    if len(fieldNameParts) == 2 :
        fieldType = fieldNameParts[0]
        fieldNumber = fieldNameParts[1]
    else :
        return True

    if fieldType == 'part':
        testPart = TestPart.objects.get(pk=fieldNumber)
        if testPart.answer == value :
            TestPartAnswer.objects.get_or_create(testPart=testPart, \
                                                 user=userId)
            return True
        else :
            return False

    elif fieldType == 'test' :
        test = Test.objects.get(pk=fieldNumber)
        if test.answer == value :
            TestAnswer.objects.get_or_create(test=test, \
                                             user=userId)
            return True
        else :
            return False

def stats(request):
    users = User.objects.filter(first_name='set').order_by('id')
    data = {}
    tests = Test.objects.all().order_by('id')
    testTitles = map(lambda test: test.title, tests)
    data['tests'] = testTitles
    data['users'] = []
    for user in users:
        userContainer = []
        userData = []
        for test in tests:
            testAnswer = TestAnswer.objects.filter(user=user.id, test=test.id)
            if testAnswer :
                testAnswer = testAnswer[0].time - user.date_joined
            else:
                testAnswer = ''
            userData.append( str(testAnswer) )
        userContainer.append(user.username)
        userContainer.append(userData)
        data['users'].append(userContainer)
        data['users'].sort(key=lambda user: user[1][-1])
    context = { 'data' : data }
    return render(request, 'stats.html', context)

@login_required
def start(request):
    request.user.date_joined = datetime.datetime.now()
    request.user.first_name = "set"
    request.user.save()
    return redirect('/')

@login_required
def tests(request):
    tests = Test.objects.all().order_by("order")
    activated = True
    finished = True
    for test in tests:
        test.activated = activated
        test.totalParts = test.testpart_set.filter().count()
        test.solvedParts = TestPartAnswer.objects \
        .filter(user = request.user) \
        .filter(
            testPart__in=test.testpart_set.all()
        ).count()
        if activated :
            if test.testanswer_set.filter(user=request.user).count() > 0 :
                test.solved = True
                activated = True
            else :
                test.solved = False
                activated = False
                finished = False
        else :
            finished = False
    context = { \
               'tests' : tests, \
               'finished': finished
    }
    return render(request, "tests.html", context)

@login_required
def test(request, test_id):
    test = Test.objects.get(pk=test_id)
    testParts = test.testpart_set.all().order_by("order")
    if test.testanswer_set.filter(user=request.user).count() > 0 :
        test.solved = True
    else :
        test.solved = False
    for testPart in testParts:
        if testPart.testpartanswer_set.filter(user=request.user).count() > 0 :
            testPart.solved = True
        else :
            testPart.solved = False
    context = { \
               'test' : test, \
               'testParts' : testParts \
    }
    if request.method == 'GET' :
        return render(request, "test.html", context)
    else :
        for postElement, value in request.POST.iteritems():
            validateField(request.user, postElement, value)
        return render(request, "test.html", context)
