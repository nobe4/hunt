from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.tests, name='tests'),
    url(r'^start$', views.start, name='start'),
    url(r'^stats$', views.stats, name='stats'),
    url(r'^test/(?P<test_id>[0-9]+)$', views.test, name='test'),
]
