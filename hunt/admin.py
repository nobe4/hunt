from django.contrib import admin

from .models import Test, TestPart, TestAnswer, TestPartAnswer

admin.site.register(Test)
admin.site.register(TestAnswer)
admin.site.register(TestPart)
admin.site.register(TestPartAnswer)
